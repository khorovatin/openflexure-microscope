# Illumination PCB

As of version 7, the OpenFlexure Microscope uses a PCB rather than just an LED.  More information, and comparison data, is in the [illumination optics explanation].  The PCB can be previewed and ordered via [kitspace], and all design files are available in its [repository].  We hope this will soon be available through suppliers such as [labmaker].  If you are unable to get hold of the PCB, it is possible to mount a 5mm LED into the condenser instead, using the [instructions for the LED workaround].

[illumination optics explanation]: ../../info_pages/illumination_optics_explanation.md
[repository]: https://gitlab.com/openflexure/openflexure-constant-current-illumination
[kitspace]: https://kitspace.org/boards/gitlab.com/openflexure/openflexure-constant-current-illumination/ofm_cc_illumination_single/
[labmaker]: https://www.labmaker.org/
[instructions for the LED workaround]:  ../../workaround_5mm_led/workaround_5mm_led.md