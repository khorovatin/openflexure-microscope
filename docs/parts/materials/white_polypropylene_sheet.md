---
PartData:
    Specs:
        Thickness: <= 0.5mm
    Suppliers:
        Kitronik:
            PartNo: 43142
            Link: https://kitronik.co.uk/collections/polypropylene-sheets/products/43142-white-polypropolene-sheet-05mm-x-1020mm-x-720mm
---
# White polypropylene sheet, 0.5mm thick

The condenser assembly needs a diffuser, which can be cut with scissors or a scalpel out of a thin sheet of white plastic.  It could be laser cut, but the edge quality is not important.  We have tested 0.5mm white polypropylene, but any white plastic sheet should do provided it's thin enough to be translucent but not transparent.  Probably 0.5mm is the maximum thickness that would be OK, and thinner would be better down to 0.1 or 0.2mm.

If you laser cut the plastic, you must observe any safety precautions necessary for your machine, e.g. ensure the plastic is compatible and won't release harmful fumes.