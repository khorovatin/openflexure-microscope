# Using a 5mm LED instead of the illumination PCB

The new condenser assembly is designed for use with an illumination PCB, containing a white LED and constant-current driver.  It is still possible to use a 5mm LED, mounted onto the lid of the condenser, and this page contains instructions for that mounting.  It requires, in addition to the parts needed for the regular condenser, two extra self tapping screws, the 5mm LED and resistor, and the condenser led holder.

{{BOM}}

* [Solder the LED](./solder_led.md){step}
* [Print the LED holder](./print_led_holder.md){step}
* [Mount the LED into the condenser lid](./mount_5mm_led.md){step}
* Continue assembling the condenser as described in the main instructions.