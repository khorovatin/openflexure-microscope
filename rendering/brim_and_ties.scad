use <../openscad/libs/main_body_structure.scad>
use <../openscad/libs/z_axis.scad>
use <../openscad/libs/utilities.scad>
use <../openscad/libs/libdict.scad>
use <librender/assembly_parameters.scad>
use <../openscad/libs/microscope_parameters.scad>
use <librender/render_settings.scad>
use <librender/rendered_main_body.scad>


render_brim_and_ties();

module render_brim_and_ties(){
    params = default_params();
    smart_brim_r = key_lookup("smart_brim_r", params);

    color(remove_colour()){
        render(6){
            exterior_brim(r=smart_brim_r, brim_only=true){
                main_body(params);
            }
        }
    }
    color(remove_colour()){
        xy_leg_ties(params);
    }
    color(remove_colour()){
        xy_actuators(params, ties_only=true);
    }
    color(remove_colour()){
        z_actuator_column(params, ties_only=true);
    }
    color(body_colour()){
        render(6){
            rendered_main_body();
        }
    }
}